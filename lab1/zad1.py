#### Exercise 1

#Input the comma-separated integers and transform them into a list of integers.
#Print the number of even ints in the given list. Note: the % "mod" operator computes the remainder, e.g. 5 % 2 is 1.
#Use a function for counting evens.

#        count_evens([2, 1, 2, 3, 4]) → 3
#        count_evens([2, 2, 0]) → 3
#        count_evens([1, 3, 5]) → 0

def funkcija(list):

    #b = len(list)

    a = 0

    for i in list:
        if i % 2 == 0:
            a+=1

    return a


list = []

stuff = input("Unesite integere odvojenim zarezom: ")

print(type(stuff))

list = [int(item) for item in stuff.split(',')]


z = funkcija(list)

print("\nBroj parnih je " + str(z))

