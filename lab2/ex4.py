import csv
import json

path1 = 'D:\Student\labsTS\se-labs-2223-sr\lab2\ex2-text.csv'

with open(path1) as f:
    reader = csv.reader(f)
    lst = list(reader)

employees = []


x = len(lst)

for i in range(x):
    if i!=0:
        print(lst[i][0])

for i in range(x):
    if i!=0:
        employee = {}
        employee['employee'] = lst[i][0]
        employee['title'] = lst[i][1]
        employee['age'] = lst[i][2]
        employee['office'] = lst[i][3]
        employees.append(employee)


print(employees)

with open('D:\Student\labsTS\se-labs-2223-sr\lab2\ex4-employees.json', 'w', encoding='utf-8') as f:
    json.dump(employees, f)