
name = input("Molimo Vas unesite ime: ")

#7.1.1.
print(f'Hi {name!s}!')

#7.1.2.
print('Hi {}!'.format(name))

#7.1.3.
print("Hi %s!" % name)